'use strict';
module.exports = function (grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*!\n' +
                ' * Competitiebeheer KNVBDataservice JavaScript library v<%= pkg.version %>\n' +
                ' * (c) Competitiebeheer.nl (A. Brockötter) - <%= pkg.homepage %>\n' +
                ' * License: <%= pkg.licenses[0].type %> (<%= pkg.licenses[0].url %>)\n' +
                ' */\n\n',
		requirejs: {
			compile: {
				options: {
				    appDir: "solution/KNVBDataservice.Client/V3/scripts/",
					 baseUrl: ".",
					 dir: "solution/KNVBDataservice.Client/V3/build/",
					 mainConfigFile: 'solution/KNVBDataservice.Client/V3/scripts/app.js',
					 namespace: 'cood',
					 optimize: "uglify2",
					 paths: {
						requireLib: 'lib/require'
					 },
					 modules: [
					 {
						 name: "knvbdataservice.extensions-<%= pkg.version %>",
						 insertRequire: ["app"],
						 include: ["requireLib", "app", "app/main"],
						 create: true
					 }
					]
				}
			}
		},
		file_append: {
			default_options: {
				files: [
					{
					prepend: "<%= banner %>",
					input: "solution/KNVBDataservice.Client/V3/build/knvbdataservice.extensions-<%= pkg.version %>.js",
					output: "solution/KNVBDataservice.Client/V3/build/knvbdataservice.extensions-<%= pkg.version %>.js"
					}
				]
			}
		}	
	});
		
	grunt.loadNpmTasks("grunt-contrib-requirejs");
	grunt.loadNpmTasks('grunt-file-append');
	
	grunt.registerTask("default", ['requirejs', 'file_append']);
};